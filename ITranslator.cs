namespace RavSoft.GoogleTranslator
{
    public interface ITranslator
    {
        /// <summary>
        /// Language used to input, what will be translated
        /// </summary>
        string SourceLanguage { set; }

        /// <summary>
        /// Language used to output, what will be resulting language
        /// </summary>
        string TargetLanguage { set; }

        string TranslateText(string text);
    }
}