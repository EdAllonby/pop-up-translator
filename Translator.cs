using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RavSoft.GoogleTranslator
{
    public class Translator : WebResourceProvider, ITranslator
    {
        private static Dictionary<string, string> languages;

        public string SourceLanguage { private get; set; }
        public string TargetLanguage { private get; set; }

        private string SourceText { get; set; }
        private string Translation { get; set; }

        public static IEnumerable<string> LanguagesAvailable
        {
            get
            {
                EnsureInitialized();
                return languages.Keys.OrderBy(p => p);
            }
        }

        public Translator()
        {
            SourceLanguage = "English";
            TargetLanguage = "French";
            Referer = "http://www.google.com";
        }

        public string TranslateText(string text)
        {
            if (!LanguageIsAvailable())
            {
                throw new Exception("An invalid source or target language was specified.");
            }

            SourceText = text;
            fetchResource();
            return Translation;
        }

        private bool LanguageIsAvailable()
        {
            return !string.IsNullOrEmpty(SourceLanguage) &&
                   !string.IsNullOrEmpty(TargetLanguage) &&
                   !SourceLanguage.Trim().Equals(TargetLanguage.Trim());
        }

        protected override string getFetchUrl()
        {
            return "http://translate.google.com/translate_t";
        }

        /// <summary>
        ///     Retrieves the POST data (if any) to be sent to the url to be fetched.
        ///     The data is returned as a string of the form "arg=val[&arg=val]...".
        /// </summary>
        /// <returns>A string containing the POST data or null if none.</returns>
        protected override string getPostData()
        {

            // Set translation mode
            string strPostData = string.Format("hl=en&ie=UTF8&oe=UTF8submit=Translate&langpair={0}|{1}",
                LanguageEnumToIdentifier(SourceLanguage),
                LanguageEnumToIdentifier(TargetLanguage));

            // Set text to be translated
            strPostData += "&text=" + HttpUtility.UrlEncode(SourceText);
            return strPostData;
        }

        /// <summary>
        ///     Parses the fetched content.
        /// </summary>
        protected override void parseContent()
        {
            RavSoft.StringParser parser = CreateParserFromContentString(Content);
            String strTranslation = ScrapeTranslation(parser);
            CleanStringTranslation(strTranslation);
        }

        private StringParser CreateParserFromContentString(string content)
        {
            Translation = string.Empty;
            var parser = new StringParser(content);
            return parser;
        }

        private static string ScrapeTranslation(StringParser parser)
        {
            string stringTranslation = string.Empty;
            if (parser.skipToEndOf("<span id=result_box"))
            {
                while (parser.skipToEndOf("onmouseout=\"this.style.backgroundColor='#fff'\">"))
                {
                    string translatedSegment = string.Empty;
                    if (parser.extractTo("</span>", ref translatedSegment))
                    {
                        stringTranslation += " " + StringParser.removeHtml(translatedSegment);
                    }
                }
            }
            return stringTranslation;
        }

        private void CleanStringTranslation(string translationString)
        {
            int startClean = PositionToStartClean(translationString);
            int endClean = PositionToEndClean(translationString);

            Translation = translationString.Substring(startClean, endClean - startClean + 1).Replace("\"", "");
        }

        private static int PositionToStartClean(string strTranslation)
        {
            int i = 0;
            int startClean = 0;

            while (i < strTranslation.Length)
            {
                if (Char.IsLetterOrDigit(strTranslation[i]))
                {
                    startClean = i;
                    break;
                }
                i++;
            }
            return startClean;
        }

        private static int PositionToEndClean(string strTranslation)
        {
            int i = strTranslation.Length - 1;
            int endClean = 0;
            while (i > 0)
            {
                char ch = strTranslation[i];
                if (Char.IsLetterOrDigit(ch) ||
                    (Char.IsPunctuation(ch) && (ch != '\"')))
                {
                    endClean = i;
                    break;
                }
                i--;
            }
            return endClean;
        }

        /// <summary>
        ///     Converts a language to its identifier.
        /// </summary>
        /// <param name="language">The language."</param>
        /// <returns>The identifier or <see cref="string.Empty" /> if none.</returns>
        private static string LanguageEnumToIdentifier
            (string language)
        {
            string mode;
            EnsureInitialized();
            languages.TryGetValue(language, out mode);
            return mode;
        }

        private static void EnsureInitialized()
        {
            if (languages == null)
            {
                languages = new Dictionary<string, string>
                {
                    {"Afrikaans", "af"},
                    {"Albanian", "sq"},
                    {"Arabic", "ar"},
                    {"Armenian", "hy"},
                    {"Azerbaijani", "az"},
                    {"Basque", "eu"},
                    {"Belarusian", "be"},
                    {"Bengali", "bn"},
                    {"Bulgarian", "bg"},
                    {"Catalan", "ca"},
                    {"Chinese", "zh-CN"},
                    {"Croatian", "hr"},
                    {"Czech", "cs"},
                    {"Danish", "da"},
                    {"Dutch", "nl"},
                    {"English", "en"},
                    {"Esperanto", "eo"},
                    {"Estonian", "et"},
                    {"Filipino", "tl"},
                    {"Finnish", "fi"},
                    {"French", "fr"},
                    {"Galician", "gl"},
                    {"German", "de"},
                    {"Georgian", "ka"},
                    {"Greek", "el"},
                    {"Haitian Creole", "ht"},
                    {"Hebrew", "iw"},
                    {"Hindi", "hi"},
                    {"Hungarian", "hu"},
                    {"Icelandic", "is"},
                    {"Indonesian", "id"},
                    {"Irish", "ga"},
                    {"Italian", "it"},
                    {"Japanese", "ja"},
                    {"Korean", "ko"},
                    {"Lao", "lo"},
                    {"Latin", "la"},
                    {"Latvian", "lv"},
                    {"Lithuanian", "lt"},
                    {"Macedonian", "mk"},
                    {"Malay", "ms"},
                    {"Maltese", "mt"},
                    {"Norwegian", "no"},
                    {"Persian", "fa"},
                    {"Polish", "pl"},
                    {"Portuguese", "pt"},
                    {"Romanian", "ro"},
                    {"Russian", "ru"},
                    {"Serbian", "sr"},
                    {"Slovak", "sk"},
                    {"Slovenian", "sl"},
                    {"Spanish", "es"},
                    {"Swahili", "sw"},
                    {"Swedish", "sv"},
                    {"Tamil", "ta"},
                    {"Telugu", "te"},
                    {"Thai", "th"},
                    {"Turkish", "tr"},
                    {"Ukrainian", "uk"},
                    {"Urdu", "ur"},
                    {"Vietnamese", "vi"},
                    {"Welsh", "cy"},
                    {"Yiddish", "yi"}
                };
            }
        }
    }
}