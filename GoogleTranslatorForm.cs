﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using MovablePython;

namespace RavSoft.GoogleTranslator
{
    public partial class GoogleTranslatorForm : Form
    {
        public GoogleTranslatorForm()
        {
            InitializeComponent();
        }

        private void GoogleTranslatorFormLoad(object sender, EventArgs e)
        {
            if (Translator.LanguagesAvailable != null)
            {
                translateFrom.Items.AddRange(Translator.LanguagesAvailable.ToArray());
                translateTo.Items.AddRange(Translator.LanguagesAvailable.ToArray());
            }
            translateFrom.SelectedItem = "English";
            translateTo.SelectedItem = "Chinese";

            PlaceLowerRight();


        }

        private void PlaceLowerRight()
        {
            //Determine "rightmost" screen
            Screen rightmost = Screen.AllScreens[0];
            foreach (Screen screen in Screen.AllScreens)
            {
                if (screen.WorkingArea.Right > rightmost.WorkingArea.Right)
                {
                    rightmost = screen;
                }
            }

            Left = rightmost.WorkingArea.Right - Width;
            Top = rightmost.WorkingArea.Bottom - Height;
        }

        private void TranslateText(object sender, EventArgs e)
        {
            // Initialize the translator
            ITranslator translator = new Translator
            {
                SourceLanguage = translateFrom.SelectedItem.ToString(),
                TargetLanguage = translateTo.SelectedItem.ToString(),
            };

            editTarget.Text = string.Empty;
            editTarget.Update();
            editReverseTranslation.Text = string.Empty;
            editReverseTranslation.Update();

            // Translate the text
            try
            {
                // Forward translation
                Cursor = Cursors.WaitCursor;
                currentProgramStatus.Text = "Translating...";
                currentProgramStatus.Update();

                editTarget.Text = translator.TranslateText(editSourceText.Text);
                editTarget.Update();

                // Reverse translation
                currentProgramStatus.Text = "Reverse translating...";
                currentProgramStatus.Update();

                Thread.Sleep(500); // let Google breathe
                translator.SourceLanguage = translateTo.SelectedItem.ToString();
                translator.TargetLanguage = translateFrom.SelectedItem.ToString();
                editReverseTranslation.Text = translator.TranslateText(editTarget.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                currentProgramStatus.Text = string.Empty;
                Cursor = Cursors.Default;
            }
        }

        private void GoogleTranslatorForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                notifyIcon1.BalloonTipText = "My application still working...";
                notifyIcon1.BalloonTipTitle = "My Sample Application";

                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(3000);
                Hide();
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            WindowState = FormWindowState.Normal;
            Show();
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            Show();
        }

        private void GoogleTranslatorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            notifyIcon1.Visible = false;
        }

        private void GoogleTranslatorForm_Paint(object sender, PaintEventArgs e)
        {
            LinearGradientBrush br = new LinearGradientBrush(this.ClientRectangle, Color.Black, Color.Black, 0, false);
            ColorBlend cb = new ColorBlend();
            cb.Positions = new[] { 0, 1 / 6f, 2 / 6f, 3 / 6f, 4 / 6f, 5 / 6f, 1 };
            cb.Colors = new[] { Color.Red, Color.Orange, Color.Yellow, Color.Green, Color.Blue, Color.Indigo, Color.Violet };
            br.InterpolationColors = cb;
            // rotate
            br.RotateTransform(45);
            // paint
            e.Graphics.FillRectangle(br, this.ClientRectangle);
        }
    }
}