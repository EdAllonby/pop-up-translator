﻿namespace RavSoft.GoogleTranslator
{
    partial class GoogleTranslatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoogleTranslatorForm));
            this.label1 = new System.Windows.Forms.Label();
            this.translateFrom = new System.Windows.Forms.ComboBox();
            this.translateTo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.editSourceText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.editTarget = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.editReverseTranslation = new System.Windows.Forms.TextBox();
            this._btnTranslate = new System.Windows.Forms.Button();
            this.currentProgramStatus = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Source language:";
            // 
            // translateFrom
            // 
            this.translateFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.translateFrom.FormattingEnabled = true;
            this.translateFrom.Location = new System.Drawing.Point(12, 78);
            this.translateFrom.MaxDropDownItems = 20;
            this.translateFrom.Name = "translateFrom";
            this.translateFrom.Size = new System.Drawing.Size(156, 21);
            this.translateFrom.Sorted = true;
            this.translateFrom.TabIndex = 1;
            // 
            // translateTo
            // 
            this.translateTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.translateTo.FormattingEnabled = true;
            this.translateTo.Location = new System.Drawing.Point(188, 78);
            this.translateTo.MaxDropDownItems = 20;
            this.translateTo.Name = "translateTo";
            this.translateTo.Size = new System.Drawing.Size(156, 21);
            this.translateTo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(188, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Target language:";
            // 
            // editSourceText
            // 
            this.editSourceText.Location = new System.Drawing.Point(12, 122);
            this.editSourceText.Multiline = true;
            this.editSourceText.Name = "editSourceText";
            this.editSourceText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.editSourceText.Size = new System.Drawing.Size(332, 60);
            this.editSourceText.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Source text:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(507, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Target text";
            // 
            // editTarget
            // 
            this.editTarget.Location = new System.Drawing.Point(507, 59);
            this.editTarget.Multiline = true;
            this.editTarget.Name = "editTarget";
            this.editTarget.ReadOnly = true;
            this.editTarget.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.editTarget.Size = new System.Drawing.Size(224, 60);
            this.editTarget.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(507, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Reverse translation:";
            // 
            // editReverseTranslation
            // 
            this.editReverseTranslation.Location = new System.Drawing.Point(507, 164);
            this.editReverseTranslation.Multiline = true;
            this.editReverseTranslation.Name = "editReverseTranslation";
            this.editReverseTranslation.ReadOnly = true;
            this.editReverseTranslation.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.editReverseTranslation.Size = new System.Drawing.Size(224, 60);
            this.editReverseTranslation.TabIndex = 9;
            // 
            // _btnTranslate
            // 
            this._btnTranslate.Location = new System.Drawing.Point(380, 122);
            this._btnTranslate.Name = "_btnTranslate";
            this._btnTranslate.Size = new System.Drawing.Size(75, 23);
            this._btnTranslate.TabIndex = 10;
            this._btnTranslate.Text = "Translate";
            this._btnTranslate.UseVisualStyleBackColor = true;
            this._btnTranslate.Click += new System.EventHandler(this.TranslateText);
            // 
            // currentProgramStatus
            // 
            this.currentProgramStatus.AutoSize = true;
            this.currentProgramStatus.Location = new System.Drawing.Point(8, 304);
            this.currentProgramStatus.Name = "currentProgramStatus";
            this.currentProgramStatus.Size = new System.Drawing.Size(10, 13);
            this.currentProgramStatus.TabIndex = 11;
            this.currentProgramStatus.Text = " ";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // GoogleTranslatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 259);
            this.Controls.Add(this.currentProgramStatus);
            this.Controls.Add(this._btnTranslate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.editReverseTranslation);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.editTarget);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.editSourceText);
            this.Controls.Add(this.translateTo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.translateFrom);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "GoogleTranslatorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Google Translator Demo";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GoogleTranslatorForm_FormClosed);
            this.Load += new System.EventHandler(this.GoogleTranslatorFormLoad);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.GoogleTranslatorForm_Paint);
            this.Resize += new System.EventHandler(this.GoogleTranslatorForm_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox translateFrom;
        private System.Windows.Forms.ComboBox translateTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox editSourceText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox editTarget;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox editReverseTranslation;
        private System.Windows.Forms.Button _btnTranslate;
        private System.Windows.Forms.Label currentProgramStatus;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}